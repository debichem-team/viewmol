Source: viewmol
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Drew Parsons <dparsons@debian.org>,
           Graham Inggs <ginggs@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-python,
               libgl1-mesa-dev | libgl-dev,
               libglu1-mesa-dev | libglu-dev,
               libmotif-dev,
               libpng-dev,
               libtiff5-dev,
               libx11-dev,
               libxext-dev,
               libxi-dev,
               libxmu-dev,
               libxt-dev,
               python-dev,
               zlib1g-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debichem-team/viewmol
Vcs-Git: https://salsa.debian.org/debichem-team/viewmol.git
Homepage: http://viewmol.sourceforge.net/

Package: viewmol
Architecture: any
Depends: ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Suggests: openbabel,
          xfonts-cyrillic
Description: graphical front end for computational chemistry programs
 Viewmol is able to graphically aid in the generation of molecular
 structures for computations and to visualize their results.
 .
 At present Viewmol includes input filters for Discover, DMol3, Gamess,
 Gaussian 9x/03, Gulp, Mopac, PQS, Turbomole, and Vamp outputs as well as
 for PDB files. Structures can be saved as Accelrys' car-files, MDL files,
 and Turbomole coordinate files. Viewmol can generate input files for
 Gaussian 9x/03. Viewmol's file format has been added to OpenBabel so that
 OpenBabel can serve as an input as well as an output filter for
 coordinates.
