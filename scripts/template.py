#!/usr/bin/python
#*******************************************************************************
#                                                                              *
#                                   Viewmol                                    *
#                                                                              *
#                            T E M P L A T E . P Y                             *
#                                                                              *
#                 Copyright (c) Joerg-R. Hill, October 2003                    *
#                                                                              *
#*******************************************************************************
#
# $Id: template.py,v 1.1 2003/11/07 13:20:26 jrh Exp $
# $Log: template.py,v $
# Revision 1.1  2003/11/07 13:20:26  jrh
# Initial revision
#
#
import viewmol

class template:
  def run(self):
    print "This is a script template which just prints this silly message."

  def register(self, language):
    viewmol.registerMenuItem("Script template")

# The following three lines are not necessary for running this script
# from the "Run script" menu, but allow the script to also be run standalone
if __name__ == '__main__':
  script=template()
  script.run()
