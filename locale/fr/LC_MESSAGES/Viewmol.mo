��          �      ,      �     �     �     �     �     �     �     �  �   �     _     w     �     �  	   �     �  F   �        =       K     T     \  	   d     n     �     �  �   �     K     h     �     �  	   �  
   �  S   �     )                         	         
                                            Ambient Cancel Color Confirm Confirm and Save Emission Module Missing Module Pmw (Python Mega Widgets) not found. Please install it before running this script again. It can be found at http://pmw.sourceforge.net/. Resource File Not Found Select element Set Atom Colors Set atom colors Shininess Specular The resource file, viewmolrc, cannot be found. Saving is not possible. Transparency Project-Id-Version: Viewmol 2.4
POT-Creation-Date: 2003-05-29 16:08+0200
PO-Revision-Date: 2003-07-27 15:35+0200
Last-Translator: Ludovic Douillard <douillard@DRECAM.CEA.fr>
Language-Team: French <douillard@DRECAM.CEA.fr>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit
 Ambiante Annuler Couleur Confirmer Confirmer et Sauvegarder Emission Module manquant Module Pmw (Python Mega Widgets) non trouv�. Veuillez l'installer avant toute nouvelle ex�cution de ce scripte. Il est disponible � l'adresse http://pmw.sourceforge.net/. Fichier Ressource non trouv� S�lectionner un �l�ment D�finir les couleurs de l'atome D�finir les couleurs de l'atome Brillance Sp�culaire Le fichier ressource, viewmolrc, est introuvable. Aucune sauvegarde n'est possible. Transparence 