/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                            R E A D F R A M E . C                             *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: readframe.c,v 1.6 2004/08/29 15:00:33 jrh Exp $
* $Log: readframe.c,v $
* Revision 1.6  2004/08/29 15:00:33  jrh
* Release 2.4.1
*
* Revision 1.5  2003/11/07 11:13:18  jrh
* Release 2.4
*
* Revision 1.4  2000/12/10 15:15:02  jrh
* Release 2.3
*
* Revision 1.3  1999/05/24 01:27:10  jrh
* Release 2.2.1
*
* Revision 1.2  1999/02/07 21:55:39  jrh
* Release 2.2
*
* Revision 1.1  1998/01/26 00:35:17  jrh
* Initial revision
*
*/
#include<ctype.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAXLENLINE 84
#define FALSE 0
#define TRUE  1

int readFrame(FILE *mainfile, int pbc, int first, double conv, int version,
		  long *offset)
{
  double energy;
  long start;
  static int cycle=0;
  int realEnd=FALSE, na=0, iend;
  char line[MAXLENLINE], *word, *w;

  *offset=ftell(mainfile);
  fgets(line, MAXLENLINE, mainfile);
  if (first)
  {
    printf("$title\n");
    /* Cutoff spaces */
    iend=strlen(line) > 65 ? 64 : strlen(line)-1;
    word=&line[iend];
    while (*word == ' ' || *word == '\n') word--;
    word++;
    *word='\0';
    word=&line[0];
    while (*word == ' ') word++;
    printf("%s\n", word);
    fgets(line, MAXLENLINE, mainfile);
  }
  else
  {
    word=line+64;
    /* Energies in car/arc files are supposed to be in thermochemical kcal/mol */
    energy=atof(word)/627.50959;
    cycle++;
    printf("  cycle = %d    SCF energy = %17.10f   |dE/dxyz| =  0.000000\n", cycle, energy);
  }
/*if (first)
  {
    printf("$coord %12.8f\n", conv);
    conv=(double)1.0/conv;
  }*/
  while (fgets(line, MAXLENLINE, mainfile) != NULL)
  {
    if (strstr(line, "end") == NULL && line[0] != '!')
    {
      if (strstr(line, "PBC") == &line[0])
      {
        if (first)
        {
          printf("$unitcell %s\n", strtok(line, "(")+3);
	  printf("$symmetry %s\n", strtok(NULL, ")"));
        }
        else
          printf("  unitcell %s\n", strtok(line, "(")+3);
      }
      else
      {
        if (first && na == 0)
        {
          printf("$coord %12.8f\n", conv);
          conv=(double)1.0/conv;
        }
        word=strtok(line+6, " \t");
        printf("%22.14f ", conv*atof(word));
        if (strlen(word) > 15)
        {
          if ((w=strchr(word, '.')) != NULL) word=w+10;
          else word+=16;
        }
        else
          word=strtok(NULL, " \t");
        printf("%22.14f ", conv*atof(word));
        if (strlen(word) > 15)
        {
          if ((w=strchr(word, '.')) != NULL) word=w+10;
          else word+=16;
        }
        else
          word=strtok(NULL, " \t");
        printf("%22.14f ", conv*atof(word));    
        word=strtok(NULL, " \t");
        word=strtok(NULL, " \t");
        word=strtok(NULL, " \t");
        if (version < 2)
          printf("%s\n", word);
        else
        {
          word=strtok(NULL, " \t");
          printf("%s\n", word);
        }
        if (realEnd) realEnd=FALSE;
        na++;
      }
    }
    else
    {
      if (realEnd)
      {
        if (!first)
        {
          while (na--) printf("%22.14e%22.14e%22.14e\n", 0.0, 0.0, 0.0);
        }
	  start=ftell(mainfile);
        if (fgets(line, MAXLENLINE, mainfile) != NULL)
	  {
	    (void)fseek(mainfile, start, SEEK_SET);
          return(TRUE);
        }
        else
          return(FALSE);
      }
      else
        realEnd=TRUE;
    }
  }
  return(TRUE);
}
