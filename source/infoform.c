/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             I N F O F O R M . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: infoform.c,v 1.2 2003/11/07 11:03:43 jrh Exp $
* $Log: infoform.c,v $
* Revision 1.2  2003/11/07 11:03:43  jrh
* Release 2.4
*
* Revision 1.1  2000/12/10 15:09:10  jrh
* Initial revision
*
*/
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include<Xm/BulletinB.h>
#include<Xm/Form.h>
#include<Xm/Label.h>
#include<Xm/LabelG.h>
#include<Xm/MessageB.h>
#include<Xm/PanedW.h>
#include<Xm/PushB.h>
#include<Xm/PushBG.h>
#include<Xm/RowColumn.h>
#include<Xm/Separator.h>
#include<Xm/Text.h>
#include<Xm/ToggleB.h>

#include<stdio.h>
#include<stdlib.h>

#include "dialog.h"

extern Widget CreatePushButtonRow(Widget, struct PushButtonRow *, int);
extern void   MapBox(Widget, caddr_t, XmAnyCallbackStruct *);
extern Widget initShell(Widget, char *, Widget *, Widget *);

void infoForm(void);
void infoFormExit(Widget, caddr_t, caddr_t);
void infoSetText(char *);

static Widget dialog, text=NULL;
static XmTextPosition position=0;
extern Widget topShell;

void infoForm(void)
{
  Widget board, form, scrolledWindow;
  static struct PushButtonRow buttons[] = {
    { "cancel", infoFormExit, (XtPointer)0, NULL },
  };                                    

  dialog=initShell(topShell, "infoForm", &board, &form);
  XtAddCallback(dialog, XmNpopupCallback, (XtCallbackProc)MapBox, (XmAnyCallbackStruct *)NULL);
  text=XmCreateScrolledText(form, "text", NULL, 0);
  scrolledWindow=XtParent(text);
  XtVaSetValues(scrolledWindow, XmNtopAttachment, XmATTACH_FORM,
                                XmNleftAttachment, XmATTACH_FORM,
                                XmNrightAttachment, XmATTACH_FORM,
                                NULL);
  XtVaSetValues(text, XmNeditMode, XmMULTI_LINE_EDIT,
                      XmNcolumns, 80,
                      XmNrows, 10,
                      XmNeditable, False,
                      XmNwordWrap, True,
                      XmNcursorPositionVisible, False,
                      XmNscrollHorizontal, False,
                      NULL);
  position=0;
  CreatePushButtonRow(form, buttons, XtNumber(buttons));

  XtManageChild(text);
  XtManageChild(form);
  XtManageChild(board);
}

void infoFormExit(Widget button, caddr_t dummy, caddr_t call_data)
{
  text=NULL;
  XtDestroyWidget(dialog);
}

void infoSetText(char *str)
{
  if (text == NULL) infoForm();
 
  XmTextInsert(text, position, str);
  position+=strlen(str);
}
