/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             O P T I F O R M . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: optiform.c,v 1.6 2003/11/07 11:08:11 jrh Exp $
* $Log: optiform.c,v $
* Revision 1.6  2003/11/07 11:08:11  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:13:26  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:51  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:54:04  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:58  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:42:48  jrh
* Initial revision
*
*/
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include<Xm/BulletinB.h>
#include<Xm/Form.h>
#include<Xm/Label.h>
#include<Xm/LabelG.h>
#include<Xm/PanedW.h>
#include<Xm/PushB.h>
#include<Xm/PushBG.h>
#include<Xm/RowColumn.h>
#include<Xm/Separator.h>
#include<Xm/ToggleB.h>
#include "viewmol.h"
#include "dialog.h"

void GetDiagram(Widget, caddr_t, XmToggleButtonCallbackStruct *);
Widget CreateToggleBox(Widget, struct PushButtonRow *, int, int,
                       int, int, int);
void optimizationExit(Widget, caddr_t, XmPushButtonCallbackStruct *);

extern void MapBox(Widget, caddr_t, XmToggleButtonCallbackStruct *);
extern Widget CreatePushButtonRow(Widget, struct PushButtonRow *, int);
extern void redraw(int);
extern void setMenuItem(int, int);
extern Widget initShell(Widget, char *, Widget *, Widget *);

extern struct WINDOW windows[];
extern Widget topShell;
static Widget dialog;
static int optset_save;

void optimizationDialog(Widget widget, caddr_t dummy, XmAnyCallbackStruct *data)
{
  Widget form, board, form1, checkbox, sep, button;
  static struct PushButtonRow buttons[] = {
    { "ok", optimizationExit, (XtPointer)TRUE, NULL },
    { "cancel", optimizationExit, (XtPointer)FALSE, NULL },
  };
  static struct PushButtonRow checkbox_buttons[] = {
    { "energies", GetDiagram, (XtPointer)ENERGY, NULL },
    { "norms", GetDiagram, (XtPointer)GNORM, NULL },
  };

  /* This function creates the dialog for the optimization history window */

  setMenuItem(HISTORY_SETTINGS, False);

  optset_save=windows[HISTORY].mode;

  dialog=initShell(windows[HISTORY].widget, "optimizationForm",
			 &board, &form);

  form1=XtVaCreateWidget("controlarea", xmFormWidgetClass, form, NULL);
  sep=XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, form, NULL);

  checkbox=CreateToggleBox(form1, checkbox_buttons, XtNumber(checkbox_buttons),
                           XmVERTICAL, 1, False, (windows[HISTORY].mode & 0x3));
  XtVaSetValues(checkbox, XmNleftAttachment, XmATTACH_FORM,
                          XmNrightAttachment, XmATTACH_FORM,
                          XmNtopAttachment, XmATTACH_FORM,
                          NULL);
  sep=XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, form1,
                              XmNtraversalOn, False,
                              XmNleftAttachment, XmATTACH_FORM,
                              XmNrightAttachment, XmATTACH_FORM,
                              XmNtopAttachment, XmATTACH_WIDGET,
                              XmNtopWidget, checkbox,
                              NULL);
  button=XtVaCreateManagedWidget("scales", xmToggleButtonWidgetClass, form1,
                                 XmNleftAttachment, XmATTACH_FORM,
                                 XmNtopAttachment, XmATTACH_WIDGET,
                                 XmNtopWidget, sep,
                                 XmNbottomAttachment, XmATTACH_FORM,
                                 NULL);
  if (windows[HISTORY].mode & SCALES) XtVaSetValues(button, XmNset, True, NULL);
  XtAddCallback(button, XmNvalueChangedCallback, (XtCallbackProc)GetDiagram, (XtPointer)SCALES);
  XtManageChild(form1);

  CreatePushButtonRow(form, buttons, XtNumber(buttons));
  XtAddCallback(dialog, XmNpopupCallback, (XtCallbackProc)MapBox, (XmAnyCallbackStruct *)NULL);
  XtManageChild(form);
  XtManageChild(board);
}

Widget CreateToggleBox(Widget parent, struct PushButtonRow *buttons,
                       int num_buttons, int orientation, int columns,
                       int radio, int deflt)
{
  Widget rc;
  register int i;

  rc=XtVaCreateWidget("rowcolumn", xmRowColumnWidgetClass, parent,
                      XmNorientation, orientation,
                      XmNradioBehavior, radio,
                      XmNnumColumns, (short)columns,
                      NULL);
  for (i=0; i<num_buttons; i++)
  {
    buttons[i].widget=XtVaCreateManagedWidget(buttons[i].label, xmToggleButtonWidgetClass,
                                              rc, NULL);
    if (radio)
    {
      if (i == deflt) XtVaSetValues(buttons[i].widget, XmNset, True, NULL);
    }
    else
    {
      if ((deflt >> i) & 0x1) XtVaSetValues(buttons[i].widget, XmNset, True, NULL);
    }
    if (buttons[i].callback)
      XtAddCallback(buttons[i].widget, XmNvalueChangedCallback,
                   (XtCallbackProc)buttons[i].callback, buttons[i].client_data);
  }
  XtManageChild(rc);
  return(rc);
}

void GetDiagram(Widget button, caddr_t which, XmToggleButtonCallbackStruct *data)
{
  if (data->set)
  {
    windows[HISTORY].mode |= (int)which;
    if (button) XtVaSetValues(button, XmNset, True, NULL);
  }
  else
  {
    windows[HISTORY].mode &= ~((int)which);
    if (button) XtVaSetValues(button, XmNset, False, NULL);
  }
  redraw(HISTORY);
}

void optimizationExit(Widget button, caddr_t which, XmPushButtonCallbackStruct *data)
{
  if (!(int)which)
    windows[HISTORY].mode=optset_save;
  XtDestroyWidget(dialog);
  setMenuItem(HISTORY_SETTINGS, True);
  redraw(HISTORY);
}
