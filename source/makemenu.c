/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             M A K E M E N U . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: makemenu.c,v 1.6 2003/11/07 11:05:39 jrh Exp $
* $Log: makemenu.c,v $
* Revision 1.6  2003/11/07 11:05:39  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:10:32  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:08  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:50:51  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:17  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:41:41  jrh
* Initial revision
*
*/
#include<Xm/MainW.h>
#include<Xm/PanedW.h>
#include<Xm/RowColumn.h>
#include<Xm/DrawingA.h>
#include<Xm/CascadeBG.h>
#include<Xm/ToggleB.h>
#include<Xm/ToggleBG.h>
#include<Xm/PushB.h>
#include<Xm/PushBG.h>
#include<Xm/ScrolledW.h>
#include "menu.h"

extern Widget topShell;
extern int rgbMode;

/* Build popup, option and pulldown menus, depending on the menu_type.
 * It may be XmMENU_PULLDOWN, XmMENU_OPTION or  XmMENU_POPUP.  Pulldowns
 * return the CascadeButton that pops up the menu.  Popups return the menu.
 * Option menus are created, but the RowColumn that acts as the option
 * "area" is returned unmanaged. (The user must manage it.)
 * Pulldown menus are built from cascade buttons, so this function
 * also builds pullright menus.  The function also adds the right
 * callback for PushButton or ToggleButton menu items.
 */
Widget makeMenu(Widget parent, int menu_type, char *label,
                struct MenuItem *items)
{
  Visual *vi;
  Colormap colormap;
  Pixel bg;
  Widget menu, cascade=NULL;
  Arg args[3];
  register int i;

  XtVaGetValues(topShell, XmNvisual, &vi, XmNcolormap, &colormap,
                XmNbackground, &bg, NULL);
  XtSetArg(args[0], XmNvisual, vi);
  XtSetArg(args[1], XmNcolormap, colormap);
  if (menu_type == XmMENU_PULLDOWN || menu_type == XmMENU_OPTION)
    menu=XmCreatePulldownMenu(parent, "_pulldown", args, 2);
  else if (menu_type == XmMENU_POPUP)
    menu=XmCreatePopupMenu(parent, "_popup", args, 2);
  else
  {
    XtWarning("Invalid menu type passed to makeMenu()");
    return(NULL);
  }

  /* Pulldown menus require a cascade button to be made */
  if (menu_type == XmMENU_PULLDOWN)
  {
    cascade=XtVaCreateManagedWidget(label,
            xmCascadeButtonGadgetClass, parent,
            XmNsubMenuId, menu,
            NULL);
  }
  else if (menu_type == XmMENU_OPTION)
  {
    /* Option menus are a special case, but not hard to handle */
    XtSetArg(args[0], XmNsubMenuId, menu);

    /* This really isn't a cascade, but this is the widget handle
       we're going to return at the end of the function. */

    cascade=XmCreateOptionMenu(parent, label, args, 1);
  }

  /* Now add the menu items */
  for (i = 0; items[i].label != NULL; i++)
  {
    /* If subitems exist, create the pull-right menu by calling this
       function recursively.  Since the function returns a cascade
       button, the widget returned is used. */

    if (items[i].subitems)
      if (menu_type == XmMENU_OPTION)
      {
        XtWarning("You can't have submenus from option menu items.");
        continue;
      }
      else
        items[i].widget=makeMenu(menu, XmMENU_PULLDOWN, items[i].label,
                                 items[i].subitems);
    else
        items[i].widget=XtVaCreateManagedWidget(items[i].label,
                        *items[i].class, menu,
                        NULL);

    if (items[i].callback)
      XtAddCallback(items[i].widget, (items[i].class == &xmToggleButtonWidgetClass ||
                    items[i].class == &xmToggleButtonGadgetClass) ?
                    XmNvalueChangedCallback : /* ToggleButton class */
                    XmNactivateCallback,      /* PushButton class */
                    items[i].callback, items[i].callback_data);
  }

 /* for popup menus, just return the menu; pulldown menus, return
    the cascade button; option menus, return the thing returned
    from XmCreateOptionMenu().  This isn't a menu, or a cascade button! */

  return(menu_type == XmMENU_POPUP? menu : cascade);
}
