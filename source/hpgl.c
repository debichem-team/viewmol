/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                                 H P G L . C                                  *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: hpgl.c,v 1.6 2003/11/07 11:02:47 jrh Exp $
* $Log: hpgl.c,v $
* Revision 1.6  2003/11/07 11:02:47  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:08:55  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:25:49  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:50:09  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:47:56  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:41:12  jrh
* Initial revision
*
*/
#include<math.h>
#include<stdio.h>
#include<X11/Xlib.h>
#include<GL/gl.h>
#include "viewmol.h"
#include "dialog.h"

extern char *textPointer;

static double fsx, fsy, stipple[8*sizeof(GLushort)];
static GLfloat pointSize, xold, yold;
static int nstipple=0, lastpen=0;

FILE *hpglInit(char *, double, double, double, double, double, double,
               int);
void hpglClose(FILE *);
void hpglSetRGBColor(FILE *, GLfloat, GLfloat, GLfloat);
void hpglMoveto(FILE *, int, GLfloat, GLfloat);
void hpglLineto(FILE *, int, GLfloat, GLfloat);
void hpglTriangle(FILE *, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
void hpglTriangleColor(FILE *, GLfloat, GLfloat, GLfloat);
void hpglPoint(FILE *, int, GLfloat, GLfloat);
void hpglLineStipple(FILE *, GLushort);
void hpglShowString(FILE *, int, GLfloat, GLfloat);

FILE *hpglInit(char *filename, double width, double height, double paperWidth,
               double paperHeight, double fontSizeX, double fontSizeY,
               int portrait)
{
  FILE *file;
  register double xs, ys, offset;

  if ((file=fopen(filename, "w")) == NULL) return(NULL);
  fprintf(file, "IN;\n");

  if (paperHeight*width/height > paperWidth)
  {
    xs=width;
    if (portrait)
    {
      ys=paperHeight*width/paperWidth;
      offset=0.5*(height-ys);
      fprintf(file, "SC%f,%f,0.0,%f;\n", ys+offset, offset, xs);
    }
    else
    {
      ys=paperWidth*height/paperHeight;
      offset=0.5*(height-ys);
      fprintf(file, "SC0.0,%f,%f,%f;\n", xs, offset, ys+offset);
    }
  }
  else
  {
    ys=height;
    if (portrait)
    {
      xs=paperHeight*width/paperWidth;
      offset=0.5*(width-xs);
      fprintf(file, "SC%f,0.0,%f,%f;\n", ys, offset, xs+offset);
    }
    else
    {
      xs=paperWidth*height/paperHeight;
      offset=0.5*(width-xs);
      fprintf(file, "SC%f,%f,0.0,%f;\n", offset, xs+offset, ys);
    }
  }
  fsx=fontSizeX;
  fsy=fontSizeY;
  lastpen=0;
  glGetFloatv(GL_POINT_SIZE, &pointSize);
  return(file);
}

void hpglClose(FILE *file)
{
  fprintf(file, "SP0;");
  fclose(file);
}

void hpglSetRGBColor(FILE *file, GLfloat red, GLfloat green, GLfloat blue)
{
  static GLfloat pens[16][3];
  register int i;

  for (i=0; i<lastpen; i++)
  {
    if (pens[i][0] == red && pens[i][1] == green &&
        pens[i][2] == blue)
    {
      fprintf(file, "SP%d;\n", i+1);
      return;
    }
  }
  pens[lastpen][0]=red;
  pens[lastpen][1]=green;
  pens[lastpen][2]=blue;
  fprintf(file, "SP%d;\n", lastpen+1);
  if (lastpen < 15) lastpen++;
  else              lastpen=0;
}

void hpglMoveto(FILE *file, int portrait, GLfloat x, GLfloat y)
{
  if (portrait)
    fprintf(file, "PU%f,%f;", y, x);
  else
    fprintf(file, "PU%f,%f;", x, y);
  xold=x;
  yold=y;
}

void hpglLineto(FILE *file, int portrait, GLfloat x, GLfloat y)
{
  double xv, yv;
  char pu[]="PU", pd[]="PD", *pen=pd;
  register int i, j=0, loop=TRUE;

  if (nstipple && pen == pd)
  {
    xv=x-xold;
    yv=y-yold;
    if (xv != 0.0) xv/=fabs(xv);
    if (yv != 0.0) yv/=fabs(yv);
    if (xv == 0.0 && yv == 0.0) loop=FALSE;
    while (loop)
    {
      for (i=0; i<=nstipple; i++)
      {
        xold+=stipple[i]*xv;
        yold+=stipple[i]*yv;
        if ((xold > x && xv > 0.0) || (xold < x && xv < 0.0))
        {
          xold=x;
          loop=FALSE;
        }
        if ((yold > y && yv > 0.0) || (yold < y && yv < 0.0))
        {
          yold=y;
          loop=FALSE;
        }
        if (portrait)
          j+=fprintf(file, "%s%f,%f;", pen, yold, xold);
        else
          j+=fprintf(file, "%s%f,%f;", pen, xold, yold);
        if (j > 80)
        {
          fprintf(file, "\n");
          j=0;
        }
        if (!loop) break;
        if (pen == pd)
          pen=pu;
        else
          pen=pd;
      }
    }
  }
  else
  {
    if (portrait)
      fprintf(file, "PD%f,%f;\n", y, x);
    else
      fprintf(file, "PD%f,%f;\n", x, y);
  }
}

void hpglTriangle(FILE *file, GLfloat x1, GLfloat x2, GLfloat x3,
                  GLfloat y1, GLfloat y2, GLfloat y3)
{
}

void hpglTriangleColor(FILE *file, GLfloat red, GLfloat green, GLfloat blue)
{
}

void hpglPoint(FILE *file, int portrait, GLfloat x, GLfloat y)
{
  if (portrait)
    fprintf(file, "PU%f,%f;\nPD;AA%f,%f,360,0;\n", y-pointSize*0.5, x, y, x);
  else
    fprintf(file, "PU%f,%f;\nPD;AA%f,%f,360,0;\n", x-pointSize*0.5, y, x, y);
}

void hpglLineStipple(FILE *file, GLushort style)
{
  int last;
  register int i, j;

  if (style == 0xffff) nstipple=0;
  else
  {
    j=0;
    stipple[j]=1;
    last=style & 0x1;
    for (i=1; i<8*sizeof(GLushort); i++)
    {
      if (((style & (0x1 << i)) >> i) == last)
        stipple[j]++;
      else
      {
        last=(style & (0x1 << i)) >> i;
        stipple[++j]=1;
      }
    }
    nstipple=j;
  }
}

void hpglShowString(FILE *file, int portrait, GLfloat x, GLfloat y)
{
/* This subroutine plots the string pointed to by textPointer
   scaled with sf starting at the position x, y, and 0.0. Plotting
   is done to the file pointed to by file. */

/* Character data */
  static short chr[]={
  /* ! */
  28,8,11,8,15,8,15,12,15,12,15,12,11,12,11,8,11,10,20,5,40,5,40,16,40,16,40,
  10,20,
  /* " */
  24,3,40,8,40,8,40,8,30,8,30,3,40,13,40,18,40,18,40,18,30,18,30,13,40,
  /* # */
  16,3,22,19,22,3,30,19,30,7,18,7,34,15,18,15,34,
  /* $ */
  48,3,15,7,11,7,11,14,11,14,11,18,15,18,15,18,22,18,22,14,26,14,26,7,26,7,26,
  3,30,3,30,3,36,3,36,7,40,7,40,14,40,14,40,18,36,11,40,11,11,
  /* % */
  36,3,11,18,40,3,40,10,40,10,40,10,33,10,33,3,33,3,33,3,40,11,18,18,18,18,18,
  18,11,18,11,11,11,11,11,11,18,
  /* & */
  48,18,22,14,22,14,22,14,15,14,15,10,11,10,11,7,11,7,11,3,15,3,15,3,22,3,22,
  14,30,14,30,14,36,14,36,10,40,10,40,5,36,5,36,5,30,5,30,18,11,
  /* ' */
  12,8,40,13,40,13,40,13,30,13,30,8,40,
  /* ( */
  12,6,11,3,20,3,20,3,31,3,31,6,40,
  /* ) */
  12,15,11,18,20,18,20,18,31,18,31,15,40,
  /* * */
  16,3,26,19,26,11,18,11,34,3,18,19,34,3,34,19,18,
  /* + */
  8,3,26,19,26,11,18,11,34,
  /* , */
  24,8,15,8,20,8,20,12,20,12,20,12,15,12,15,9,11,9,11,10,15,10,15,8,15,
  /* - */
  4,3,26,18,26,
  /* . */
  16,8,11,8,15,8,15,12,15,12,15,12,11,12,11,8,11,
  /* / */
  4,3,11,18,40,
  /* 0 */
  36,3,15,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,15,18,15,14,11,14,11,
  7,11,7,11,3,15,3,15,18,36,
  /* 1 */
  8,3,26,11,40,11,40,11,11,
  /* 2 */
  28,3,30,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,30,18,30,3,11,3,11,18,
  11,
  /* 3 */
  32,3,40,18,40,18,40,11,26,11,26,14,26,14,26,18,22,18,22,18,15,18,15,14,11,14,
  11,7,11,7,11,3,15,
  /* 4 */
  12,3,15,14,40,14,20,14,11,3,15,18,15,
  /* 5 */
  32,3,15,7,11,7,11,14,11,14,11,18,15,18,15,18,22,18,22,14,26,14,26,3,26,3,26,
  3,40,3,40,18,40,
  /* 6 */
  44,3,22,7,26,7,26,14,26,14,26,18,22,18,22,18,15,18,15,14,11,14,11,7,11,7,11,
  3,15,3,15,3,36,3,36,7,40,7,40,14,40,14,40,18,36,
  /* 7 */
  8,3,40,18,40,18,40,7,11,
  /* 8 */
  60,3,15,3,22,3,22,7,26,7,26,14,26,14,26,18,30,18,30,18,36,18,36,14,40,14,40,
  7,40,7,40,3,36,3,36,3,30,3,30,7,26,14,26,18,22,18,22,18,15,18,15,14,11,14,11,
  7,11,7,11,3,15,
  /* 9 */
  40,3,15,7,11,7,11,14,11,14,11,18,15,18,15,18,36,18,36,14,40,14,40,7,40,7,40,
  3,36,3,36,3,30,3,30,7,26,7,26,18,26,
  /* : */
  32,8,25,8,30,8,30,12,30,12,30,12,25,12,25,8,25,8,15,8,20,8,20,12,20,12,20,12,
  15,12,15,8,15,
  /* ; */
  40,8,25,8,30,8,30,12,30,12,30,12,25,12,25,8,25,8,15,8,20,8,20,12,20,12,20,12,
  15,12,15,9,11,9,11,10,15,10,15,8,15,
  /* < */
  8,10,16,3,23,3,23,10,30,
  /* = */
  8,3,29,18,29,18,23,3,23,
  /* > */
  8,11,30,18,23,18,23,11,16,
  /* ? */
  48,8,11,8,15,8,15,12,15,12,15,12,11,12,11,8,11,8,20,18,30,18,30,18,36,18,36,
  14,40,14,40,7,40,7,40,3,36,3,36,10,36,10,36,13,33,13,33,8,20,
  /* @ */
  0,
  /* A */
  24,3,11,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,11,18,26,3,26,
  /* B */
  40,3,11,3,40,3,40,14,40,14,40,18,36,18,36,18,30,18,30,14,26,14,26,3,26,14,26,
  18,22,18,22,18,15,18,15,14,11,14,11,3,11,
  /* C */
  28,18,15,14,11,14,11,7,11,7,11,3,15,3,15,3,36,3,36,7,40,7,40,14,40,14,40,18,
  36,
  /* D */
  24,3,11,3,40,3,40,14,40,14,40,18,36,18,36,18,15,18,15,14,11,14,11,3,11,
  /* E */
  16,3,11,3,40,3,40,18,40,14,26,3,26,3,11,18,11,
  /* F */
  12,3,11,3,40,3,40,18,40,14,26,3,26,
  /* G */
  36,11,26,18,26,18,26,18,15,18,15,14,11,14,11,7,11,7,11,3,15,3,15,3,36,3,36,
  7,40,7,40,14,40,14,40,18,36,
  /* H */
  12,3,11,3,40,18,40,18,11,18,26,3,26,
  /* I */
  12,7,11,14,11,11,11,11,40,7,40,14,40,
  /* J */
  16,3,15,7,11,7,11,14,11,14,11,18,15,18,15,18,40,
  /* K */
  16,3,11,3,40,18,40,11,26,11,26,3,26,11,26,18,11,
  /* L */
  8,3,40,3,11,3,11,18,11,
  /* M */
  16,3,11,3,40,3,40,11,26,11,26,18,40,18,40,18,11,
  /* N */
  12,3,11,3,40,3,40,18,11,18,11,18,40,
  /* O */
  32,3,15,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,15,18,15,14,11,14,11,
  7,11,7,11,3,15,
  /* P */
  24,3,11,3,40,3,40,14,40,14,40,18,36,18,36,18,30,18,30,14,26,14,25,3,26,
  /* Q */
  36,3,15,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,15,18,15,14,11,14,11,
  7,11,7,11,3,15,14,15,18,11,
  /* R */
  28,3,11,3,40,3,40,14,40,14,40,18,36,18,36,18,30,18,30,14,26,14,26,3,26,14,
  26,18,11,
  /* S */
  44,3,15,7,11,7,11,14,11,14,11,18,15,18,15,18,22,18,22,14,26,14,26,7,26,7,
  26,3,30,3,30,3,36,3,36,7,40,7,40,14,40,14,40,18,36,
  /* T */
  8,11,11,11,40,3,40,18,40,
  /* U */
  20,3,40,3,15,3,15,7,11,7,11,14,11,14,11,18,15,18,15,18,40,
  /* V */
  8,3,40,11,11,11,11,18,40,
  /* W */
  24,3,40,3,15,3,15,7,11,7,11,11,15,11,15,14,11,14,11,18,15,18,15,18,40,
  /* X */
  8,3,40,18,11,3,11,18,40,
  /* Y */
  12,3,40,11,26,11,26,18,40,11,26,11,11,
  /* Z */
  12,3,40,18,40,18,40,3,11,3,11,18,11,
  /* [ */
  0,
  /* \ */
  4,3,40,18,11,
  /* ] */
  0,
  /* ^ */
  0,
  /* _ */
  4,3,11,18,11,
  /* ` */
  0,
  /* a */
  32,3,15,3,22,3,22,7,26,7,26,14,26,14,26,18,22,18,26,18,11,18,15,14,11,14,
  11,7,11,7,11,3,15,
  /* b */
  32,3,11,3,40,3,22,7,26,7,26,14,26,14,26,18,22,18,22,18,15,18,15,14,11,14,
  11,7,11,7,11,3,15,
  /* c */
  28,18,15,14,11,14,11,7,11,7,11,3,15,3,15,3,22,3,22,7,26,7,26,14,26,14,26,
  18,22,
  /* d */
  32,18,15,14,11,14,11,7,11,7,11,3,15,3,15,3,22,3,22,7,26,7,26,14,26,14,26,
  18,22,18,40,18,11,
  /* e */
  36,18,15,14,11,14,11,7,11,7,11,3,15,3,15,3,22,3,22,7,26,7,26,14,26,14,26,
  18,22,18,22,18,18,18,18,3,18,
  /* f */
  20,7,11,7,36,7,36,11,40,11,40,14,40,14,40,18,36,11,26,3,26,
  /* g */
  44,3,5,7,1,7,1,14,1,14,1,18,5,18,5,18,26,18,22,14,26,14,26,7,26,7,26,3,22,
  3,22,3,15,3,15,7,11,7,11,14,11,14,11,18,15,
  /* h */
  20,3,11,3,40,3,22,7,26,7,26,14,26,14,26,18,22,18,22,18,11,
  /* i */
  16,7,11,14,11,11,11,11,26,11,26,7,26,11,31,11,34,
  /* j */
  20,5,5,9,1,9,1,12,1,12,1,16,5,16,5,16,26,16,31,16,34,
  /* k */
  12,3,11,3,40,18,27,3,19,3,19,18,11,
  /* l */
  12,7,11,14,11,11,11,11,40,11,40,7,40,
  /* m */
  28,3,11,3,26,3,22,7,26,7,26,11,22,11,22,11,11,11,22,15,26,15,26,19,22,19,
  22,19,11,
  /* n */
  20,3,11,3,26,3,22,7,26,7,26,14,26,14,26,18,22,18,22,18,11,
  /* o */
  32,3,15,3,22,3,22,7,26,7,26,14,26,14,26,18,22,18,22,18,15,18,15,14,11,14,
  11,7,11,7,11,3,15,
  /* p */
  32,3,1,3,26,3,22,7,26,7,26,14,26,14,26,18,22,18,22,18,15,18,15,14,11,14,
  11,7,11,7,11,3,15,
  /* q */
  32,18,15,14,11,14,11,7,11,7,11,3,15,3,15,3,22,3,22,7,26,7,26,14,26,14,
  26,18,22,18,26,18,1,
  /* r */
  16,3,11,3,26,3,22,7,26,7,26,14,26,14,26,18,22,
  /* s */
  36,3,15,7,11,7,11,14,11,14,11,18,15,18,15,14,19,14,19,6,19,6,19,3,22,3,
  22,7,26,7,26,14,26,14,26,18,22,
  /* t */
  20,18,15,14,11,14,11,10,11,10,11,6,15,6,15,6,40,3,26,9,26,
  /* u */
  20,3,26,3,15,3,15,7,11,7,11,14,11,14,11,18,15,18,11,18,26,
  /* v */
  12,3,26,10,11,10,11,11,11,11,11,18,26,
  /* w */
  24,3,26,3,15,3,15,7,11,7,11,11,15,11,15,15,11,15,11,19,15,19,15,19,26,
  /* x */
  8,3,26,18,11,3,11,18,26,
  /* y */
  32,3,5,7,1,7,1,14,1,14,1,18,5,18,5,18,26,3,26,3,15,3,15,7,11,7,11,14,
  11,14,11,18,15,
  /* z */
  12,3,26,18,26,18,26,3,11,3,11,18,11,
  /* { */
  0,
  /* | */
  4,10,11,10,40,
  /* } */
  0,
  /* ~ */
  0,
  /* � */
  32,3,29,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,29,18,29,14,25,14,
  25,7,25,7,25,3,29,
  /* � (Adiaeresis) */
  32,3,11,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,11,18,26,3,26,7,
  43,7,46,14,43,14,46,
  /* � (Odiaeresis) */
  40,3,15,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,15,18,15,14,11,
  14,11,7,11,7,11,3,15,7,43,7,46,14,43,14,46,
  /* � (Udiaeresis) */
  28,3,40,3,15,3,15,7,11,7,11,14,11,14,11,18,15,18,15,18,40,7,43,7,46,14,
  43,14,46,
  /* � (ssharp) */
  44,3,1,3,36,3,36,7,40,7,40,14,40,14,40,18,36,18,36,18,30,18,30,14,26,14,
  26,11,26,14,26,18,22,18,22,18,15,18,15,14,11,14,11,7,11,
  /* � (adiaeresis) */
  40,3,15,3,22,3,22,7,26,7,26,14,26,14,26,18,22,18,26,18,11,18,15,14,11,
  14,11,7,11,7,11,3,15,7,28,7,31,14,28,14,31,
  /* � (odiaeresis) */
  40,3,15,3,22,3,22,7,26,7,26,14,26,14,26,18,22,18,22,18,15,18,15,14,11,
  14,11,7,11,7,11,3,15,7,28,7,31,14,28,14,31,
  /* � (udiaeresis) */
  28,3,26,3,15,3,15,7,11,7,11,14,11,14,11,18,15,18,11,18,26,7,28,7,31,14,
  28,14,31};
  double x1, y1;
  int chrstart;
  unsigned char ic, *p;
  register int j, k;

  switch (textPointer[0])
  {
    /* text shall be output right aligned at position x, y */
    case 'r': x-=22.*fsx*(double)strlen(textPointer+1);
              break;
    /* text shall be output centered at position x, y */
    case 'c': x-=11.*fsx*(double)strlen(textPointer+1);
              break;
  }

/* now print text one letter at a time */
  for (p=(unsigned char *)textPointer+1; *p!=(unsigned char)0; p++)
  {
    if (*p != ' ')
    {
      ic=(*p)-'!';
      if (ic > 93)
      {
        switch (ic)
        {
          case 143: ic=94;
                    break;
          case 163: ic=95;
                    break;
          case 181: ic=96;
                    break;
          case 187: ic=97;
                    break;
          case 190: ic=98;
                    break;
          case 195: ic=99;
                    break;
          case 213: ic=100;
                    break;
          case 219: ic=101;
                    break;
        }
      }
      chrstart=0;
      k=0;
      for (j=0; j<ic; j++)
      {
        chrstart+=chr[k]+1;
        k+=chr[k]+1;
      }
      for (j=1; j<chr[chrstart]; j+=4)
      {
        x1=x+fsx*chr[chrstart+j];
        y1=y+fsy*chr[chrstart+j+1];
        if (portrait)
          fprintf(file, "PU%f,%f;", y1, x1);
        else
          fprintf(file, "PU%f,%f;", x1, y1);
        x1=x+fsx*chr[chrstart+j+2];
        y1=y+fsy*chr[chrstart+j+3];
        if (portrait)
          fprintf(file, "PD%f,%f;\n", y1, x1);
        else
          fprintf(file, "PD%f,%f;\n", x1, y1);
      }
    }
    x+=22.*fsx;
  }
  textPointer+=strlen(textPointer)+1;
}
