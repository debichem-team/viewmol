/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               W I N D O W . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: window.c,v 1.6 2003/11/07 11:17:31 jrh Exp $
* $Log: window.c,v $
* Revision 1.6  2003/11/07 11:17:31  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:19:04  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:27:59  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:59:50  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:49:49  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:44:28  jrh
* Initial revision
*
*/
#include<X11/Xlib.h>
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include "viewmol.h"

void setWindowTitle(Widget widget, char *title)
{
  if (title != NULL)
    XtVaSetValues(XtParent(widget), XmNtitle, title, NULL);

/*XTextProperty text;

  if (XStringListToTextProperty(&title, 1, &text))
  {
    XSetWMName(XtDisplay(widget), XtWindow(widget), &text);
    XFlush(XtDisplay(widget));
  } */
}
