/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                              V I E W M O L . H                               *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: viewmol.h,v 1.7 2004/08/29 15:02:31 jrh Exp $
* $Log: viewmol.h,v $
* Revision 1.7  2004/08/29 15:02:31  jrh
* Release 2.4.1
*
* Revision 1.6  2003/11/07 12:50:40  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:18:08  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:27:50  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:58:15  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:49:43  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:46:03  jrh
* Initial revision
*
*/
#include<X11/Intrinsic.h>
#include<GL/gl.h>
#include<GL/glx.h>
#include<Python.h>
#include "menu.h"
#include "modules.h"

#define PROGRAM "Viewmol"
#define VERSION "2.4.1"

#define MAXLENLINE 256
#define MAXLENBASISNAME 32
#define TRUE  1
#define FALSE 0

#define BLACK   0
#define WHITE   1
#define RED     2
#define GREEN   3
#define BLUE    4
#define YELLOW  5
#define CYAN    6
#define MAGENTA 7
#define SKYBLUE 8

#define VIEWER   0
#define SPECTRUM 1
#define HISTORY  2
#define MO       3
#define VIEWER1  4
#define VIEWER2  5

#define VIEWER_MOLECULE       ((VIEWER << 8) |  0)
#define VIEWER_SELECT         ((VIEWER << 8) |  1)
#define VIEWER_SEP1           ((VIEWER << 8) |  2)
#define VIEWER_WIRE           ((VIEWER << 8) |  3)
#define VIEWER_STICK          ((VIEWER << 8) |  4)
#define VIEWER_BALL           ((VIEWER << 8) |  5)
#define VIEWER_CPK            ((VIEWER << 8) |  6)
#define VIEWER_SEP2           ((VIEWER << 8) |  7)
#define VIEWER_GEOMETRY       ((VIEWER << 8) |  8)
#define VIEWER_BONDTYPE       ((VIEWER << 8) |  9)
#define VIEWER_WAVEFUNCTION   ((VIEWER << 8) | 10)
#define VIEWER_ENERGYLEVEL    ((VIEWER << 8) | 11)
#define VIEWER_OPTIMIZATION   ((VIEWER << 8) | 12)
#define VIEWER_FORCES         ((VIEWER << 8) | 13)
#define VIEWER_SPECTRUM       ((VIEWER << 8) | 14)
#define VIEWER_THERMODYNAMICS ((VIEWER << 8) | 15)
#define VIEWER_UNITCELL       ((VIEWER << 8) | 16)
#define VIEWER_INERTIA        ((VIEWER << 8) | 17)
#define VIEWER_SEP3           ((VIEWER << 8) | 18)
#define VIEWER_DRAWING        ((VIEWER << 8) | 19)
#define VIEWER_FOREGROUND     ((VIEWER << 8) | 20)
#define VIEWER_BACKGROUND     ((VIEWER << 8) | 21)
#define VIEWER_LABEL          ((VIEWER << 8) | 22)
#define VIEWER_ANNOTATE       ((VIEWER << 8) | 23)
#define VIEWER_RUNSCRIPT      ((VIEWER << 8) | 24)
#define VIEWER_SEP4           ((VIEWER << 8) | 25)
#define VIEWER_HARDCOPY       ((VIEWER << 8) | 26)
#define VIEWER_RAYTRACING     ((VIEWER << 8) | 27)
#define VIEWER_MANUAL         ((VIEWER << 8) | 28)
#define VIEWER_SEP5           ((VIEWER << 8) | 29)
#define VIEWER_CONFIGURATION  ((VIEWER << 8) | 30)
#define VIEWER_QUIT           ((VIEWER << 8) | 31)

#define VIEWER1_LOAD          ((VIEWER1 << 8) | 0)
#define VIEWER1_SAVE          ((VIEWER1 << 8) | 1)
#define VIEWER1_DELETE        ((VIEWER1 << 8) | 2)
#define VIEWER1_NEW           ((VIEWER1 << 8) | 3)
#define VIEWER1_BUILD         ((VIEWER1 << 8) | 4)

#define VIEWER2_ALL           ((VIEWER2 << 8) | 0)
#define VIEWER2_LAST          ((VIEWER2 << 8) | 1)
#define VIEWER2_SEP           ((VIEWER2 << 8) | 2)
#define VIEWER2_UNDO          ((VIEWER2 << 8) | 3)

#define SPECTRUM_SETTINGS     ((SPECTRUM << 8) |  0)
#define SPECTRUM_SELECT       ((SPECTRUM << 8) |  1)
#define SPECTRUM_IMAGINARY    ((SPECTRUM << 8) |  2)
#define SPECTRUM_OBSERVED     ((SPECTRUM << 8) |  3)
#define SPECTRUM_DELETE       ((SPECTRUM << 8) |  4)
#define SPECTRUM_ZOOMOUT      ((SPECTRUM << 8) |  5)
#define SPECTRUM_HARDCOPY     ((SPECTRUM << 8) |  6)
#define SPECTRUM_SEP1         ((SPECTRUM << 8) |  7)
#define SPECTRUM_FOREGROUND   ((SPECTRUM << 8) |  8)
#define SPECTRUM_BACKGROUND   ((SPECTRUM << 8) |  9)
#define SPECTRUM_SEP2         ((SPECTRUM << 8) | 10)
#define SPECTRUM_QUIT         ((SPECTRUM << 8) | 11)

#define HISTORY_SETTINGS      ((HISTORY << 8) | 0)
#define HISTORY_SELECT        ((HISTORY << 8) | 1)
#define HISTORY_ANIMATE       ((HISTORY << 8) | 2)
#define HISTORY_HARDCOPY      ((HISTORY << 8) | 3)
#define HISTORY_SEP1          ((HISTORY << 8) | 4)
#define HISTORY_ENERGY        ((HISTORY << 8) | 5)
#define HISTORY_GRADIENT      ((HISTORY << 8) | 6)
#define HISTORY_BACKGROUND    ((HISTORY << 8) | 7)
#define HISTORY_SEP2          ((HISTORY << 8) | 8)
#define HISTORY_QUIT          ((HISTORY << 8) | 9)

#define MO_SETTINGS           ((MO << 8) |  0)
#define MO_SELECT             ((MO << 8) |  1)
#define MO_TRANSITION         ((MO << 8) |  2)
#define MO_ZOOMOUT            ((MO << 8) |  3)
#define MO_HARDCOPY           ((MO << 8) |  4)
#define MO_ENERGYLEVELS       ((MO << 8) |  5)
#define MO_SEP1               ((MO << 8) |  6)
#define MO_FOREGROUND         ((MO << 8) |  7)
#define MO_BACKGROUND         ((MO << 8) |  8)
#define MO_SEP2               ((MO << 8) |  9)
#define MO_QUIT               ((MO << 8) | 10)

#define BACKGROUND 1
#define FOREGROUND 2

#define WIREMODEL  1
#define STICKMODEL 2
#define BALLMODEL  3
#define CUPMODEL   4

#define SPECTRUM_ALL   1
#define SPECTRUM_IR    2
#define SPECTRUM_RAMAN 3
#define SPECTRUM_INS   4

#define ANIMATE    1
#define ARROWS     2
#define DISTORT    3
#define TRAJECTORY 4

#define SCALES 0x1
#define ENERGY 0x2
#define GNORM  0x4

#define HARTREE 1
#define KJ_MOL  2
#define EV      3
#define CM      4

#define DENSITY_OF_STATES 1
#define ENERGY_LEVELS     2

#define BONDAVERAGE 1
#define BONDLENGTH  2
#define ANGLE       3
#define TORSION     4

#define ALL_OFF           1
#define BASIS_FUNCTION    2
#define BASIS_IN_MO       3
#define MOLECULAR_ORBITAL 4
#define DENSITY           5
#define GRIDREAD          0x100

#define IP_NONE    1
#define IP_LINEAR  2
#define IP_LOG     3

#define LABEL 1
#define INS   2

#define SCREEN     1
#define TIFFFILE   2
#define PNGFILE    3
#define HPGL       4
#define POSTSCRIPT 5
#define RAYTRACER  6

#define A5         1
#define A4         2
#define A3         3
#define LETTER     4
#define LEGAL      5
#define USER       6

#define ORTHO       0
#define PERSPECTIVE 1

#define LINE_STIPPLE 0x10000
#define LINE_WIDTH   0x20000
#define ADD_DEPTH    0x30000
#define VISIBILITY   0x40000

#define VIEWPOINT 0
#define LIGHTNO0  1
#define LIGHTNO1  2
#define WORLD     3
#define MOLECULES 4

#define CENTERED 1
#define COORDINATES 2

#define SINGLE_BONDS   1
#define MULTIPLE_BONDS 2
#define CONJUGATION    3

#define NOTHING 0
#define TEXT    1
#define DELETE  2

#define DRAWANN  0x1
#define EDITABLE 0x2
#define MOVEABLE 0x4
#define GEOMETRY 0x8

#define ORIGINAL     0x1
#define DISPLAY_ONLY 0x2
#define X_FIXED      0x4
#define Y_FIXED      0x8
#define Z_FIXED      0x10
#define SELECTED     0x20

#define ADD_ATOM     1
#define DELETE_ATOM  2
#define REPLACE_ATOM 3
#define CREATE_BOND  4
#define REMOVE_BOND  5
#define NEW_MOLECULE 6
#define BOND_ORDER   7

#define TRANSLATION 0x1
#define PV          0x2
#define ROTATION    0x4
#define VIBRATION   0x8
#define TOTAL       0x10

#define JOULES         0
#define CALORIES       1
#define THERMOCALORIES 2

#define REACTANT    -1
#define PRODUCT      1
#define ALLREACTIONS 2

#define REACTION_OK       0
#define MISSING_ATOMS     1
#define CANT_BALANCE      2
#define INCONSISTENT_TYPE 3

#define ALPHAANDBETA 0
#define ALPHA        1
#define BETA         2

#define ALL  -1
#define LAST -2
#define LMAX 4        /* maximum l quantum number supported */

struct BASISSET
{
  double     *first;          /* points to the first exponent of a shell  */
  int         nprim;          /* the number of primitives in a shell      */
  int         ang;            /* the angular quantum number of the shell  */
  struct BASISSET *next;      /* points to the BASISSET structure for the */
};                            /* next shell of this atom or NULL          */

struct MOLECULE
{
  struct ATOM *atoms;
  struct BOND *bonds;
  struct UNITCELL *unitcell;
  struct NORMAL_MODE *normal_modes;
  struct COORDS *history;
  struct INTERNAL *internals;
  struct OPTIMIZATION *optimization;
  struct ORBITAL *orbitals;
  struct GRIDOBJECT *gridObjects;
  struct BASISSET *basisset;
  struct ELEMENT *bondStyle;
  struct SAVE *coord;
  PyMoleculeSpecObject *pyObject;
  double bondRadius, bondShift;
  double *cnm, *exponents;
  double transx, transy, transz;
  double rotConstants[3], tinert[3][3];
  double htrl, strl, ctrl, hrot, srot, crot, hvib, svib, cvib, pv;
  double emax, emin, gmax;
  int *addedBonds, *deletedBonds;
  int cycle;
  int na, nb, nbAdded, nbDeleted, nmodes, nhist, ninternal, nbasfu, ngridobjects;
  int pured, puref, pureg, reaction, stoichioNumber;
  int mode, imo, imosave, ibasfu, imag, needMoloch, gaussian, thermoSettings;
  char title[MAXLENLINE], pgroup[6];
};

struct ATOM
{
  double x;
  double y;
  double z;
  double rad;
  double radScale;
  double mass;
  double neutronScatterfac;
  int    nbonds;
  int    hbonds;
  int    nelectrons;
  int    ref;
  int    nBasisFunctions;
  int    flags;
  struct ELEMENT  *element;
  struct BASISSET *basis;
  char   name[8];
  char   **basisFunctionNames;
  char   basisname[MAXLENBASISNAME];
}; 

struct BOND
{
  int    first;
  int    second;
  int    order;
  double frac;
  double x;
  double y;
  double z;
};

struct UNITCELL
{
  struct ATOM *corners;
  struct ATOM *millerPlanes;
  struct EDGE *edges;
  double a, b, c;
  double alpha, beta, gamma;
  double factor[3];
  int nc, ne, np;
  int miller[3], nmiller;
  int showUnitCell, showMiller;
};

struct EDGE
{
  int first;
  int second;
};

struct ELEMENT
{
  double  rad;
  double  radScale;
  double  mass;
  GLfloat dark[4];
  GLfloat light[4];
  GLfloat emission[4];
  GLfloat ambient[4];
  GLfloat specular[4];
  GLfloat shininess;
  GLfloat alpha;
  GLint   colormap[3];
  char    symbol[2];
};

struct NORMAL_MODE
{
  double wavenumber;
  double ir_intensity;
  double raman_intensity;
  double ins_intensity;
  double rel_ir_intensity;
  double rel_raman_intensity;
  double rel_ins_intensity;
  char   sym[8];
};

struct WINDOW
{
  Widget          widget;
  GLXContext      context;
  int             foreground;
  int             background;
  int             smooth;
  int             mode;
  int             set;
  Widget          menu;
  struct MENUDATA *selectMenu;
  double          left;
  double          right;
  double          bottom;
  double          top;
  double          near;
  double          far;
  int             horizontalMargin;
  int             verticalMargin;
  int             mouseX;
  int             mouseY;
  float           foreground_rgb[4];
  float           background_rgb[4];
  XFontStruct     *font;
  XFontStruct     *greekFont;
  GLuint          GLfontId;
  GLuint          GLgreekFontId;
};

struct OPTION
{
  char flag[8];
  char *identifier;
  char command[MAXLENLINE];
};

struct COORDS
{
  GLdouble x;
  GLdouble y;
  GLdouble z;
  GLdouble gx;
  GLdouble gy;
  GLdouble gz;
};

struct SAVE
{
  double x, y, z;
  char name[8];
};

struct UNDO
{
  struct SAVE *coord;
  int mol;
  int na;
};

struct INTERNAL
{
  double value;
  double x;
  double y;
  double z;
  int atoms[4];
  int type;
};

struct OPTIMIZATION
{
  GLdouble energy;
  GLdouble gnorm;
  int      coords;
  int      natoms;
};

struct ORBITAL
{
  GLdouble energy;
  GLdouble occupation;
  double  *coeff;
  int      spin;
  char     symmetry[8];
};

struct GRIDOBJECT
{
  double origin[3];
  double vector1[3], vector2[3], vector3[3];
  double start[3], step[3];
  double resolution;
  double *grid;
  int    npoints[3];
  int    ngridpoints[3];
  int    type;
  int    mo;
  int    basisfunction;
  char   symmetry[8];
  char   text[MAXLENLINE];
};

struct ZOOM
{
  int    window;
  double left;
  double right;
  double bottom;
  double top;
};

struct TOGGLE
{
  int *var;
  int  value;
  void (*draw)();
};

struct SLIDER
{
  double *number;
  int    decimals;
  void   (*draw)();
};

struct ANNOTATION
{
  PyLabelSpecObject *pyObject;
  Widget widget;
  double x;
  double y;
  double z;
  float  color_rgb[4];
  int    color;
  int    flags;
  int    userdata;
  char   text[MAXLENLINE];
};
