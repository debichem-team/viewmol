/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               R E D R A W . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: redraw.c,v 1.6 2003/11/07 11:14:53 jrh Exp $
* $Log: redraw.c,v $
* Revision 1.6  2003/11/07 11:14:53  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:15:58  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:27:26  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:56:17  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:49:22  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:43:41  jrh
* Initial revision
*
*/
#include<Xm/Xm.h>
#include<GL/glx.h>
#include "viewmol.h"

extern void showTitle(Widget,  caddr_t, XmDrawingAreaCallbackStruct *);
extern void drawMolecule(Widget, caddr_t, XmDrawingAreaCallbackStruct *);
extern void drawSpectrum(Widget, caddr_t, XmDrawingAreaCallbackStruct *);
extern void drawMODiagram(Widget, caddr_t, XmDrawingAreaCallbackStruct *);
extern void drawHistory(Widget, caddr_t, XmDrawingAreaCallbackStruct *);

extern struct MOLECULE *molecules;

void redraw(int window)
{
  switch (window)
  {
    case VIEWER:   if (molecules == NULL)
                     showTitle((Widget)0, (caddr_t)0, (XmDrawingAreaCallbackStruct *)0);
                   else
                     drawMolecule((Widget)0, (caddr_t)0, (XmDrawingAreaCallbackStruct *)0);
                   break;
    case SPECTRUM: drawSpectrum((Widget)0, (caddr_t)0, (XmDrawingAreaCallbackStruct *)0);
                   break;
    case MO:       drawMODiagram((Widget)0, (caddr_t)0, (XmDrawingAreaCallbackStruct *)0);
                   break;
    case HISTORY:  drawHistory((Widget)0, (caddr_t)0, (XmDrawingAreaCallbackStruct *)0);
                   break;
  }
}
