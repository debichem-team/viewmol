/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               M E S S G B . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: messgb.c,v 1.6 2003/11/07 11:06:50 jrh Exp $
* $Log: messgb.c,v $
* Revision 1.6  2003/11/07 11:06:50  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:11:26  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:28  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:52:26  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:34  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:42:04  jrh
* Initial revision
*
*/
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include<Xm/BulletinB.h>
#include<Xm/Form.h>
#include<Xm/Label.h>
#include<Xm/LabelG.h>
#include<Xm/MessageB.h>
#include<Xm/PanedW.h>
#include<Xm/PushB.h>
#include<Xm/PushBG.h>
#include<Xm/RowColumn.h>
#include<Xm/Separator.h>
#include<Xm/Text.h>
#include<Xm/ToggleB.h>

#include<stdio.h>
#include<stdlib.h>

#include "dialog.h"

Widget CreatePushButtonRow(Widget, struct PushButtonRow *, int);
void   MapBox(Widget, caddr_t, XmAnyCallbackStruct *);

extern Widget initShell(Widget, char *, Widget *, Widget *);

static Widget dialog;
static int cont, ret;
extern XtAppContext app;
extern Widget topShell;

Widget CreatePushButtonRow(Widget parent, struct PushButtonRow *buttons, int n)
{
  Widget row;
  Dimension height, h;
  register int i;

  row=XtVaCreateManagedWidget("row", xmFormWidgetClass, parent,
                              XmNfractionBase, 20*n-1, NULL);

  for (i=0; i<n; i++)
  {
    buttons[i].widget=XtVaCreateManagedWidget(buttons[i].label, xmPushButtonGadgetClass, row,
                                              XmNleftAttachment, i ? XmATTACH_POSITION : XmATTACH_FORM,
                                              XmNleftPosition, 20*i,
                                              XmNtopAttachment, XmATTACH_FORM,
                                              XmNbottomAttachment, XmATTACH_FORM,
                                              XmNrightAttachment, i != n-1 ? XmATTACH_POSITION: XmATTACH_FORM,
                                              XmNrightPosition, 20*i+19,
                                              XmNshowAsDefault, i==0,
                                              XmNdefaultButtonShadowThickness, 1,
                                              NULL);
    if (buttons[i].callback)
      XtAddCallback(buttons[i].widget, XmNactivateCallback,
                    (XtCallbackProc)buttons[i].callback,
                    buttons[i].client_data);
    if (i==0)
    {
      XtVaGetValues(row, XmNmarginHeight, &h, NULL);
      XtVaGetValues(buttons[i].widget, XmNheight, &height, NULL);
      height+=2*h;
      XtVaSetValues(row, XmNdefaultButton, buttons[i].widget, XmNpaneMaximum,
                    height, XmNpaneMinimum, height, NULL);
    }
  }
  return(row);
}

void MapBox(Widget form, caddr_t dummy, XmAnyCallbackStruct *cbs)
{
  Display *display;
  Window root, child;
  int rootX, rootY, windowX, windowY, screen;
  unsigned int mask;
  Dimension w, h, rootW, rootH;

  display=XtDisplay(form);
  screen=DefaultScreen(display);
  XQueryPointer(display, XtWindow(form), &root, &child, &rootX, &rootY,
                &windowX, &windowY, &mask);
  XtVaGetValues(form, XmNwidth, &w, XmNheight, &h, NULL);
  windowX=rootX-w/2;
  windowY=rootY-h/2;
  if (windowX < 0) windowX=0;
  if (windowY < 0) windowY=0;
  rootW=DisplayWidth(display, screen);
  rootH=DisplayHeight(display, screen);
  if (windowX+w > rootW) windowX=rootW-w;
  if (windowY+h > rootH) windowY=rootH-h;
  XtVaSetValues(form, XmNx, windowX, XmNy, windowY, XmNminWidth, w,
                XmNminHeight, h, NULL); 
}

int messgb(Widget parent, int serious, char *str,
           struct PushButtonRow *buttons, int n)
{
  XEvent event;
  static Widget form;
  Widget form1, board, sign, label, sep;
  Pixmap pixmap;
  Pixel fg, bg;
  XmString text;
  int depth;
#include "messgb.h"

  dialog=initShell(parent, "messageForm", &board, &form);
  XtAddCallback(dialog, XmNpopupCallback, (XtCallbackProc)MapBox, (XmAnyCallbackStruct *)NULL);
  form1=XtVaCreateWidget("form1", xmFormWidgetClass, form, NULL);
  sep=XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, form, NULL);
  text=XmStringCreateLtoR(str, XmSTRING_DEFAULT_CHARSET);
  if (serious != 0)
  {
    XtVaGetValues(form1, XmNforeground, &fg, XmNbackground, &bg, NULL);
    XtVaGetValues(parent, XtNdepth, &depth, NULL);
    pixmap=XCreatePixmapFromBitmapData(XtDisplay(form1), RootWindowOfScreen(XtScreen(form1)),
                                       (char *)sign_data[serious-1], 64, 64, fg, bg, depth);
    sign=XtVaCreateManagedWidget("sign", xmLabelWidgetClass, form1,
                                  XmNlabelType, XmPIXMAP,
                                  XmNlabelPixmap, pixmap,
                                  XmNtopAttachment, XmATTACH_FORM,
                                  XmNleftAttachment, XmATTACH_FORM,
                                  XmNbottomAttachment, XmATTACH_FORM,
                                  NULL);
    label=XtVaCreateManagedWidget("text", xmLabelWidgetClass, form1,
                                   XmNlabelString, text,
                                   XmNtopAttachment, XmATTACH_FORM,
                                   XmNleftAttachment, XmATTACH_WIDGET,
                                   XmNleftWidget, sign,
                                   XmNrightAttachment, XmATTACH_FORM,
                                   XmNbottomAttachment, XmATTACH_FORM,
                                   NULL);
  }
  else
    label=XtVaCreateManagedWidget("text", xmLabelWidgetClass, form1,
                                   XmNlabelString, text,
                                   XmNtopAttachment, XmATTACH_FORM,
                                   XmNleftAttachment, XmATTACH_FORM,
                                   XmNrightAttachment, XmATTACH_FORM,
                                   XmNbottomAttachment, XmATTACH_FORM,
                                   NULL);
  CreatePushButtonRow(form, buttons, n);

  XtManageChild(form1);
  XtManageChild(form);
  XtManageChild(board);

  cont=TRUE;
  while (cont)
  {
    XtAppNextEvent(app, &event);
    XtDispatchEvent(&event);
  }
  return(ret);
}

void GetMessageBoxButton(Widget button, XtPointer which, caddr_t call_data)
{
  ret=(int)which;
  cont=FALSE;
  XtDestroyWidget(dialog);
}
