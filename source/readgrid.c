/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             R E A D G R I D . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: readgrid.c,v 1.1 2003/11/07 11:14:21 jrh Exp $
* $Log: readgrid.c,v $
* Revision 1.1  2003/11/07 11:14:21  jrh
* Initial revision
*
*/
#include<errno.h>
#include<math.h>
#include<stdio.h>
#include<X11/Intrinsic.h>
#include "viewmol.h"
#include "dialog.h"

extern void GetMessageBoxButton(Widget, XtPointer, caddr_t);
extern char *getStringResource(Widget, char *);
extern int  messgb(Widget, int, char *, struct PushButtonRow *, int);
extern void *getmem(size_t, size_t);

extern Widget topShell;

void readGrid(struct GRIDOBJECT *grid, FILE *file, char *line)
{
  int data=FALSE, len=0, i=0;
  char *word, format[6];

  strncpy(grid->symmetry, "A", 1);
  while ((fgets(line, MAXLENLINE, file)) != NULL)
  {
    if (strchr(line, '$')) return;
    if (strstr(line, "origin"))
    {
      (void)strtok(line, " \t");
      grid->origin[0]=atof(strtok(NULL, " \t"));
      grid->origin[1]=atof(strtok(NULL, " \t"));
      grid->origin[2]=atof(strtok(NULL, " \t"));
    }
    if (strstr(line, "vector1"))
    {
      (void)strtok(line, " \t");
      grid->vector1[0]=atof(strtok(NULL, " \t"));
      grid->vector1[1]=atof(strtok(NULL, " \t"));
      grid->vector1[2]=atof(strtok(NULL, " \t"));
    }
    if (strstr(line, "vector2"))
    {
      (void)strtok(line, " \t");
      grid->vector2[0]=atof(strtok(NULL, " \t"));
      grid->vector2[1]=atof(strtok(NULL, " \t"));
      grid->vector2[2]=atof(strtok(NULL, " \t"));
    }
    if (strstr(line, "vector3"))
    {
      (void)strtok(line, " \t");
      grid->vector3[0]=atof(strtok(NULL, " \t"));
      grid->vector3[1]=atof(strtok(NULL, " \t"));
      grid->vector3[2]=atof(strtok(NULL, " \t"));
    }
    if (strstr(line, "grid1"))
    {
      (void)strtok(line, " \t");
      (void)strtok(NULL, " \t");
      grid->start[0]=0.52917706*atof(strtok(NULL, " \t"));
      (void)strtok(NULL, " \t");
      grid->step[0]=0.52917706*atof(strtok(NULL, " \t"));
      (void)strtok(NULL, " \t");
      grid->ngridpoints[0]=(int)(atof(strtok(NULL, " \t")))-1;
      grid->npoints[0]=grid->ngridpoints[0];
    }
    if (strstr(line, "grid2"))
    {
      (void)strtok(line, " \t");
      (void)strtok(NULL, " \t");
      grid->start[1]=0.52917706*atof(strtok(NULL, " \t"));
      (void)strtok(NULL, " \t");
      grid->step[1]=0.52917706*atof(strtok(NULL, " \t"));
      (void)strtok(NULL, " \t");
      grid->ngridpoints[1]=(int)(atof(strtok(NULL, " \t")))-1;
      grid->npoints[1]=grid->ngridpoints[1];
    }
    if (strstr(line, "grid3"))
    {
      (void)strtok(line, " \t");
      (void)strtok(NULL, " \t");
      grid->start[2]=0.52917706*atof(strtok(NULL, " \t"));
      (void)strtok(NULL, " \t");
      grid->step[2]=0.52917706*atof(strtok(NULL, " \t"));
      (void)strtok(NULL, " \t");
      grid->ngridpoints[2]=(int)(atof(strtok(NULL, " \t")))-1;
      grid->npoints[2]=grid->ngridpoints[2];
    }
    if (strstr(line, "title"))
    {
      word=fgets(line, MAXLENLINE, file);
      strncpy(grid->text, word, MAXLENLINE);
    }
    if (strstr(line, "type"))
    {
      (void)strtok(line, " \t");
      word=strtok(NULL, " \t");
      if (!strcmp(word, "mo"))
      {
        grid->type=GRIDREAD | MOLECULAR_ORBITAL;
        word=strtok(NULL, " \t");
        sscanf(word, "%d%s", &(grid->mo), grid->symmetry);
      }
      else if (!strcmp(word, "density"))
        grid->type=GRIDREAD | DENSITY;
    }
    if (data)
    {
      while ((word=strchr(line, 'D')) != NULL) *word='E';
      if ((word=strrchr(line, '\n')) != NULL) *word='\0';
      word=line;
      if (len == 0)
      {
        len=strlen(line) > 76 ? 16 : 15;
        sprintf(format, "%%%.2dle", len);
      }
      while (sscanf(word, format, &grid->grid[i++]) != EOF) word+=len;
      i--;
    }
    if (strstr(line, "plotdata"))
    {
      data=TRUE;
      grid->grid=(double *)getmem((size_t)((grid->ngridpoints[0]+1)*(grid->ngridpoints[1]+1)
                                 *(grid->ngridpoints[2]+1)), sizeof(double));
      grid->resolution=(double)grid->ngridpoints[0];
      i=0;
    }
  }
}

void adjustMONumbers(struct MOLECULE *mol)
{
  register int i, j;

  for (i=0; i<mol->ngridobjects; i++)
  {
    for (j=0; j<mol->nbasfu; j++)
    {
      if (!strcmp(mol->gridObjects[i].symmetry, mol->orbitals[j].symmetry))
	{
	  if (--mol->gridObjects[i].mo == 0)
	  {
	    mol->gridObjects[i].mo=j;
	    break;
        }
      }
    }
  }
}
