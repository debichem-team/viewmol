/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             R A Y T R A C E . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: raytrace.c,v 1.6 2003/11/07 11:13:07 jrh Exp $
* $Log: raytrace.c,v $
* Revision 1.6  2003/11/07 11:13:07  jrh
* Release 2.4
*
*
* Revision 1.5  2000/12/10 15:14:42  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:27:05  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:55:23  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:49:09  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:43:29  jrh
* Initial revision
*
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<X11/Intrinsic.h>
#include<Xm/Xm.h>
#include "viewmol.h"
#include "dialog.h"

extern char *getStringResource(Widget, char *);
extern void GetMessageBoxButton(Widget, XtPointer, caddr_t);
extern char *selectFile(char *, char *, int);
extern int messgb(Widget, int, char *, struct PushButtonRow *, int);
extern char *saveVectorFile(int, char *, int);
extern int runProg(char *, int, char *, char *, char *, pid_t *);
extern int checkFile(char **);

extern Widget topShell;
extern struct WINDOW windows[];
extern pid_t raypid;
extern char raytracer[], displayImage[];

void raytrace(Widget widget, caddr_t data1, caddr_t data2)
{
  static struct PushButtonRow buttons1[] = {{"continue", GetMessageBoxButton, (XtPointer)0, NULL},
                                            {"cancel", GetMessageBoxButton, (XtPointer)1, NULL}};
  static struct PushButtonRow buttons2[] = {{"doRaytracing", GetMessageBoxButton, (XtPointer)0, NULL},
                                            {"stopRaytracing", GetMessageBoxButton, (XtPointer)1, NULL}};
  static struct PushButtonRow buttons3[] = {{"continue", GetMessageBoxButton, (XtPointer)0, NULL}};
  static char *rayfile="vm_image.ray";
  Dimension width, height;
  pid_t pid;
  char line[MAXLENLINE];
  char pngfile[MAXLENLINE], *dot;

  if ((rayfile=selectFile("*", rayfile, TRUE)) == NULL) return;
  saveVectorFile(VIEWER, rayfile, RAYTRACER);
  strcpy(pngfile, rayfile);
  if ((dot=strrchr(pngfile,'.')) != NULL)
    strcpy(dot, ".png");
  else
    strcat(pngfile, ".png");
  if (!access(pngfile, F_OK))
  {
    dot=getStringResource(topShell, "FileExists");
    sprintf(line, dot, pngfile);
    if (messgb(topShell, 1, line, buttons1, 2) == 1) return;
  }

  if (*raytracer != '\0')
  {
    if (*displayImage == '\0')
    {
      dot=getStringResource(topShell, "noDisplay");
      if (messgb(topShell, 3, dot, buttons2, 2) == 1) return;
    }
    if ((raypid=fork()) == (pid_t)0)
    {
	XtVaGetValues(windows[VIEWER].widget, XmNwidth, &width, XmNheight, &height, NULL);
      sprintf(line, raytracer, rayfile, pngfile, width, height);
      runProg(line, TRUE, NULL, NULL, "/dev/null", &pid);
/*    runProg(raytracer, TRUE, rayfile, pngfile, "/dev/null", &pid);*/
      if (*displayImage != '\0')
      {
        sprintf(line, displayImage, pngfile);
        runProg(line, FALSE, NULL, NULL, "/dev/null", &pid);
      }
      exit(0);
    }
  }
  else
  {
    dot=getStringResource(topShell, "noRayshade");
    messgb(topShell, 3, dot, buttons3, 1);
  }
}
