/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               N O R M O D . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: normod.c,v 1.7 2004/08/29 14:58:29 jrh Exp $
* $Log: normod.c,v $
* Revision 1.7  2004/08/29 14:58:29  jrh
* Release 2.4.1
*
* Revision 1.6  2003/11/07 11:08:01  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:13:19  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:49  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:53:50  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:55  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:42:41  jrh
* Initial revision
*
*/
#include<unistd.h>
#include<X11/Intrinsic.h>
#include "viewmol.h"

extern void redraw(int);
extern int  calcInternal(struct MOLECULE *, int);
extern int getIntResource(Widget, char *);

extern struct WINDOW windows[];
extern struct MOLECULE *molecules;
extern double amplitude;
extern int nmolecule;

Boolean normalMode(XtPointer data)
{
  static int j=1, k=0;
  double fac;
  int imol, mode;
  register int i, l, n;

/*if ((int)data == 1)
  {
    j=1;
    k=0;
  }*/
  fac=(double)k*amplitude*0.05;
  for (imol=0; imol<nmolecule; imol++)
  {
    n=molecules[imol].na;
    mode=molecules[imol].mode;
    if (mode != (-1))
    {
      if (molecules[imol].unitcell) n-=8;
      for (i=0; i<n; i++)
      {
        l=3*molecules[imol].atoms[i].ref;
        molecules[imol].atoms[i].x=molecules[imol].coord[i].x+fac*molecules[imol].cnm[mode+molecules[imol].nmodes*l];
        molecules[imol].atoms[i].y=molecules[imol].coord[i].y+fac*molecules[imol].cnm[mode+molecules[imol].nmodes*(l+1)];
        molecules[imol].atoms[i].z=molecules[imol].coord[i].z+fac*molecules[imol].cnm[mode+molecules[imol].nmodes*(l+2)];
      }
      for (i=0; i<molecules[imol].ninternal; i++)
        (void)calcInternal(&molecules[imol], i);
    }
  }
  if (j < 0) k--;
  else       k++;
  if (abs(k) == 5) j=-j;
  redraw(VIEWER);
  usleep(getIntResource(windows[VIEWER].widget, "delay"));
  return(False);
}
