/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               M A N U A L . C                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: manual.c,v 1.6 2003/11/07 11:06:29 jrh Exp $
* $Log: manual.c,v $
* Revision 1.6  2003/11/07 11:06:29  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:10:45  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:28:34  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:51:05  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:22  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:41:54  jrh
* Initial revision
*
*/
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#if !defined LINUX && !defined IRIX && !defined _HPUX_SOURCE && !defined DARWIN && !defined FREEBSD
#include<sys/access.h>
#endif
#include<Xm/Xm.h>
#include "viewmol.h"
#include "dialog.h"

extern Widget topShell;
extern pid_t manual_pid;
extern char webbrowser[];
extern void GetMessageBoxButton(Widget, XtPointer, caddr_t);
extern char *getStringResource(Widget, char *);
extern int messgb(Widget, int, char *, struct PushButtonRow *, int);
extern int runProg(char *, int, char *, char *, char *, pid_t *);

void manual(Widget widget, caddr_t client_data, caddr_t call_data)
{
  static struct PushButtonRow buttons[] = {{"continue", GetMessageBoxButton, (XtPointer)0, NULL}};
  char command[MAXLENLINE], manualfile[MAXLENLINE], *word;

  if (*webbrowser == '\0')
  {
    word=getStringResource(topShell, "noBrowser");
    sprintf(manualfile, word, getStringResource(topShell, "webBrowser"));
    messgb(topShell, 1, manualfile, buttons, 1);
    return;
  }
  strcpy(manualfile, getenv("VIEWMOLPATH"));
  strcat(manualfile, "/doc/viewmol.html");
  if (access(manualfile, 0))
  {
    word=getStringResource(topShell, "noManual");
    sprintf(command, word, manualfile);
    messgb(topShell, 1, command, buttons, 1);
    return;
  }
  sprintf(command, webbrowser, manualfile);

  if (runProg(command, FALSE, "/dev/null", "/dev/null", "/dev/null", &manual_pid))
  {
    word=getStringResource(topShell, "cannotDisplay");
    messgb(topShell, 1, word, buttons, 1);
  }
}
