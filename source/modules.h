/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                              M O D U L E S . H                               *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: modules.h,v 1.2 2003/11/07 12:50:04 jrh Exp $
* $Log: modules.h,v $
* Revision 1.2  2003/11/07 12:50:04  jrh
* Release 2.4
*
* Revision 1.1  2000/12/10 15:12:27  jrh
* Initial revision
*
*/
typedef struct
{
  PyObject_HEAD
  int moleculeID;
} PyMoleculeSpecObject;

typedef struct
{
  PyObject_HEAD
  int moleculeID;
  int atomID;
} PyAtomSpecObject;

typedef struct
{
  PyObject_HEAD
  int elementID;
} PyElementSpecObject;

typedef struct
{
  PyObject_HEAD
  int spectrumID;
} PySpectrumSpecObject;

typedef struct
{
  PyObject_HEAD
  int historyID;
} PyHistorySpecObject;

typedef struct
{
  PyObject_HEAD
  int energyLevelID;
} PyEnergyLevelSpecObject;

typedef struct
{
  PyObject_HEAD
  int lightID;
} PyLightSpecObject;

typedef struct
{
  PyObject_HEAD
  int labelID;
} PyLabelSpecObject;
