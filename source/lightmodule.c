/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                          L I G H T M O D U L E . C                           *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: lightmodule.c,v 1.2 2003/11/07 11:05:34 jrh Exp $
* $Log: lightmodule.c,v $
* Revision 1.2  2003/11/07 11:05:34  jrh
* Release 2.4
*
* Revision 1.1  2000/12/10 15:10:17  jrh
* Initial revision
*
*/     
#include<stdio.h>
#include<Xm/DrawingA.h>
#include<Xm/ToggleB.h>
#include "viewmol.h"

#define PyLight_API_pointers 1
#define PyLightSpec_Type_NUM 0

PyLightSpecObject *light_new(void);
static PyObject *light_rotate(PyObject *, PyObject *);
static PyObject *light_switch(PyObject *, PyObject *);
static PyObject *light_getattr(PyLightSpecObject *, char *);
static void light_dealloc(PyLightSpecObject *);

extern void getRotation(int, int, int, int);
extern int checkInterrupt(void);

extern float *rotObject;
extern int moveItem, rotateXY, rotateZ, lights;

static char PyLightSpec_Type__doc__[] =
  "Light specification";

statichere PyTypeObject PyLightSpec_Type = {
  PyObject_HEAD_INIT(NULL)
  0,                               /*ob_size*/
  "LightSpec",                     /*tp_name*/
  sizeof(PyLightSpecObject),       /*tp_basicsize*/
  0,                               /*tp_itemsize*/
  /* methods */
  (destructor)light_dealloc,       /*tp_dealloc*/
  0,                               /*tp_print*/
  (getattrfunc)light_getattr,      /*tp_getattr*/
  0,                               /*tp_setattr*/
  0,                               /*tp_compare*/
  0,                               /*tp_repr*/
  0,                               /*tp_as_number*/
  0,                               /*tp_as_sequence*/
  0,                               /*tp_as_mapping*/
  0,                               /*tp_hash*/
  0,                               /*tp_call*/
  0,                               /*tp_str*/
  0,                               /*tp_getattro*/
  0,                               /*tp_setattro*/
  /* Space for future expansion */
  0L,0L,
  /* Documentation string */
  PyLightSpec_Type__doc__
};

static PyMethodDef light_methods[] = {
  {"rotate",           light_rotate,    1},
  {"switch",           light_switch,    1},
  {NULL,               NULL}
};

static PyObject *light_rotate(PyObject *self, PyObject *args)
{
  PyLightSpecObject *s;
  int moveItem_save, which, x, y, z;

  if (checkInterrupt()) return(NULL);
  if (!PyArg_ParseTuple(args, "iii", &x, &y, &z)) return(NULL);
 
  s=(PyLightSpecObject *)self;
  which=s->lightID;
  rotObject[4*which]=rotObject[4*which+1]=rotObject[4*which+2]=0.0;
  rotObject[4*which+3]=1.0;
  moveItem_save=moveItem;
  moveItem=which;
  getRotation(x, y, z, 1);
  moveItem=moveItem_save;
  Py_INCREF(Py_None);
  return(Py_None);
}

static PyObject *light_switch(PyObject *self, PyObject *args)
{
  PyLightSpecObject *s;
  int which, i, mode;

  if (checkInterrupt()) return(NULL);
  if (!PyArg_ParseTuple(args, "i", &mode)) return(NULL);
 
  if (mode != TRUE && mode != FALSE)
  {
    PyErr_SetString(PyExc_ValueError, "Mode must be ON or OFF");
    return(NULL);
  }
  s=(PyLightSpecObject *)self;
  which=s->lightID-1;
  i=1 << which;
  if (mode)
    lights|=i;
  else
    lights&=~i;
  Py_INCREF(Py_None);
  return(Py_None);
}

PyLightSpecObject *light_new(void)
{
  PyLightSpecObject *self;

  self=PyObject_NEW(PyLightSpecObject, &PyLightSpec_Type);
  if (self == NULL)
  {
    PyErr_NoMemory();
    return(NULL);
  }
  self->lightID=0;
  return((PyLightSpecObject *)self);
}

static void light_dealloc(PyLightSpecObject *self)
{
  if (!self) return;
  PyMem_DEL(self);
}

static PyObject *light_getattr(PyLightSpecObject *self, char *name)
{
  return(Py_FindMethod(light_methods, (PyObject *)self, name));
}

void initLightModule(void)
{
  PyObject *module, *dict;
  static void *PyLight_API[PyLight_API_pointers];

  PyLightSpec_Type.ob_type=&PyType_Type;

  module=Py_InitModule("light", light_methods);
  dict=PyModule_GetDict(module);

  PyLight_API[PyLightSpec_Type_NUM]=(void *)&PyLightSpec_Type;
  PyDict_SetItemString(dict, "_C_API", PyCObject_FromVoidPtr((void *)PyLight_API, NULL));
}
