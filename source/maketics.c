/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                             M A K E T I C S . C                              *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: maketics.c,v 1.6 2003/11/07 11:06:23 jrh Exp $
* $Log: maketics.c,v $
* Revision 1.6  2003/11/07 11:06:23  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:10:39  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:26:10  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:50:58  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:48:19  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:41:45  jrh
* Initial revision
*
*/
#include<math.h>
#include<stdio.h>
#include<stdlib.h>

double dbl_raise(double x, int y)
{
  register int i;
  double val;

  val=1.0e0;
  for (i=0; i<abs(y); i++)
    val*=x;
  if (y < 0 ) return(1.0e0/val);
  return(val);
}

double makeTics(double tmin, double tmax, double *start, char *format)
{
  register double r, logr, tic;

  r=fabs(tmin-tmax);
  logr=log10(r);
  r=pow(10.0e0, logr-(double)((logr >= 0.0 ) ? (int)logr : ((int)logr-1)));
  if (r <= 2.0e0)
    tic=0.2e0;
  else if (r <= 5.0e0)
    tic=0.5e0;
  else
    tic=1.0e0;
  if (logr > 0.0e0)
    sprintf(format, "%%%dg", (int)logr+2);
  else
    sprintf(format, "%%.%dg", (int)(-logr)+(tmin == 0.0e0 ? 0 : (int)log10(fabs(tmin))+2));

  tic*=dbl_raise(10.0e0, (logr >= 0.0 ) ? (int)logr : ((int)logr-1));
  *start=tic*ceil(tmin/tic);
  return(tic);
}
