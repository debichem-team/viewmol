/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                               D I A L O G . H                                *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: dialog.h,v 1.6 2003/11/07 12:48:38 jrh Exp $
* $Log: dialog.h,v $
* Revision 1.6  2003/11/07 12:48:38  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:04:36  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:25:06  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:46:41  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:47:20  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:45:27  jrh
* Initial revision
*
*/

struct PushButtonRow
{
  char      *label;
  void      (*callback)();
  XtPointer client_data;
  Widget    widget;
};
