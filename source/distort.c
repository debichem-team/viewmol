/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                              D I S T O R T . C                               *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: distort.c,v 1.6 2003/11/07 10:59:25 jrh Exp $
* $Log: distort.c,v $
* Revision 1.6  2003/11/07 10:59:25  jrh
* Release 2.4
*
* Revision 1.5  2000/12/10 15:04:51  jrh
* Release 2.3
*
* Revision 1.4  1999/05/24 01:25:08  jrh
* Release 2.2.1
*
* Revision 1.3  1999/02/07 21:46:57  jrh
* Release 2.2
*
* Revision 1.2  1998/01/26 00:47:24  jrh
* Release 2.1
*
* Revision 1.1  1996/12/10  18:40:16  jrh
* Initial revision
*
*/
#include<stdlib.h>
#include "viewmol.h"
#include "dialog.h"

extern struct MOLECULE *molecules;
extern struct WINDOW windows[];

void distortGeometry(double);

extern int calcInternal(struct MOLECULE *, int);

extern int ninternal;

void distortGeometry(double factor)
{
  struct MOLECULE *mol;
  int mode;
  register int i, l, n;

  mol=&molecules[windows[SPECTRUM].set];
  mode=mol->mode;
  if (mode != -1)
  {
    n=mol->na;
    if (mol->unitcell) n-=8;
    for (i=0; i<n; i++)
    {
	l=3*mol->atoms[i].ref;
      mol->atoms[i].x+=factor*mol->cnm[mode+mol->nmodes*l];
      mol->atoms[i].y+=factor*mol->cnm[mode+mol->nmodes*(l+1)];
      mol->atoms[i].z+=factor*mol->cnm[mode+mol->nmodes*(l+2)];
      l+=3;
    }
    for (i=0; i<mol->ninternal; i++)
      (void)calcInternal(mol, i);
  }
}
