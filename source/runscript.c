/*******************************************************************************
*                                                                              *
*                                   Viewmol                                    *
*                                                                              *
*                            R U N S C R I P T . C                             *
*                                                                              *
*                 Copyright (c) Joerg-R. Hill, October 2003                    *
*                                                                              *
********************************************************************************
*
* $Id: runscript.c,v 1.2 2003/11/07 11:15:29 jrh Exp $
* $Log: runscript.c,v $
* Revision 1.2  2003/11/07 11:15:29  jrh
* Release 2.4
*
* Revision 1.1  2000/12/10 15:16:22  jrh
* Initial revision
*
*/
#include<stdio.h>
#include<Xm/Xm.h>
#include "viewmol.h"
#include "dialog.h"

extern char *selectFile(char *, char *, int);
extern char *getStringResource(Widget, char *);
extern int messgb(Widget, int, char *, struct PushButtonRow *, int);
extern void GetMessageBoxButton(Widget, XtPointer, caddr_t);

extern Widget topShell;
extern int pythonInterrupt;

void runScript(Widget, caddr_t, XmAnyCallbackStruct *);
void startModule(Widget, caddr_t, XmAnyCallbackStruct *);

void runScript(Widget w, caddr_t data, XmAnyCallbackStruct *dummy)
{
  FILE *file;
  static struct PushButtonRow buttons[] = {{"continue", GetMessageBoxButton, (XtPointer)0, NULL}};
  static char *filename="";
  char line[MAXLENLINE], *word;

  if ((char *)data == NULL)
    filename=selectFile("*.py", filename, TRUE);
  else
    filename=(char *)data;

  if (filename != NULL)
  {
    if ((file=fopen(filename, "r")) == NULL)
    {
      word=getStringResource(topShell, "noFile");
      sprintf(line, word, filename);
      (void)messgb(topShell, 1, line, buttons, 1);
    }
    else
    {
      pythonInterrupt=FALSE;
      PyRun_SimpleFile(file, filename);
      fclose(file);
    }
  }
}

void startModule(Widget w, caddr_t data, XmAnyCallbackStruct *dummy)
{
  char cmd[MAXLENLINE], number[4];

  sprintf(number, "%2.2d", (int)data);
  strncpy(cmd, "module", MAXLENLINE-1);
  strncat(cmd, number, MAXLENLINE-strlen(cmd));
  strncat(cmd, ".run()", MAXLENLINE-strlen(cmd)-strlen(number));
  PyRun_SimpleString(cmd);
}
